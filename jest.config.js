module.exports = {
  roots: ['<rootDir>/src'],
  collectCoverageFrom: ['<rootDir>/src/**/*.ts'],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: [
    '<rootDir>/(src)+(/[a-zA-Z0-9-_]+)+/protocols/[a-zA-Z0-9-_]+(.ts){1}',
    '<rootDir>/(src)+(/[a-zA-Z0-9-_]+)/.+(-protocols){1}(.ts){1}'
  ],
  testEnvironment: 'node',
  preset: '@shelf/jest-mongodb',
  transform: {
    '.+\\.ts$': 'ts-jest'
  }
}
